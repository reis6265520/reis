package com.tours.services;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tours.dtos.PlaceDTO;
import com.tours.models.Place;
import com.tours.repos.PlaceRepository;
import com.tours.utils.StorageService;

@Service
public class PlaceService {

	@Autowired PlaceRepository repo;
	@Autowired StorageService storage;
	
	public void savePlace(PlaceDTO dto) {
		Place place=new Place();
		BeanUtils.copyProperties(dto, place);
		place.setPhoto(storage.store(dto.getPic()));
		repo.save(place);
	}
	
	public List<Place> listAll(){
		return repo.findAll();
	}
	
	public void deletePlace(int id) {
		Place pl=repo.findById(id).get();
		storage.delete(pl.getPhoto());
		repo.deleteById(id);
	}
	
}
