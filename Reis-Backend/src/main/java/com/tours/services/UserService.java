package com.tours.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tours.dtos.LoginDTO;
import com.tours.models.User;
import com.tours.repos.UsersRepository;

@Service
public class UserService {

	@Autowired UsersRepository repo;
	
	public void saveUser(User user) {
		if(user.getRole()==null)
			user.setRole("User");
		repo.save(user);
	}
	
	public List<User> getAllUsers(){
		return repo.findAll();
	}
	
	public User findByUserId(String userid) {
		return repo.getById(userid);
	}
	
	public User ValidateLogin(LoginDTO dto) {
		Optional<User> u=repo.findById(dto.getUserid());
		if(u.isPresent() && u.get().getPwd().equals(dto.getPwd())) {
			return u.get();
		}
		else  {
			return null;
		}
	}
	
}
