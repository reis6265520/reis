package com.tours.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.tours.models.TourPackage;
import com.tours.repos.PackageRepository;
import com.tours.utils.StorageService;

@Service
public class TourPackageService {

	@Autowired
	PackageRepository repo;
	@Autowired
	StorageService storage;

	public void savePakage(TourPackage pkg, MultipartFile photo) {
		if (photo != null) {
			pkg.setPhoto(storage.store(photo));
		} else if (pkg.getPid() > 0) {
			TourPackage pp = findById(pkg.getPid());
			pkg.setPhoto(pp.getPhoto());
		}
		repo.save(pkg);
	}

	public List<TourPackage> listall() {
		return repo.findAll();
	}

	public TourPackage findById(int id) {
		return repo.getById(id);
	}

	public void deletePackage(int id) {
		TourPackage pkg = findById(id);
		repo.deleteById(id);
	}
}
