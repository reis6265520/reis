package com.tours.dtos;

import org.springframework.web.multipart.MultipartFile;

import com.tours.models.Place;

public class PlaceDTO extends Place {
	private MultipartFile pic;

	public MultipartFile getPic() {
		return pic;
	}

	public void setPic(MultipartFile pic) {
		this.pic = pic;
	}
	
	
}
