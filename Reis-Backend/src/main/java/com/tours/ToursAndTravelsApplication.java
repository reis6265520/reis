package com.tours;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import com.tours.models.User;
import com.tours.services.UserService;
import com.tours.utils.FileUploadProperties;

@SpringBootApplication
@EnableJpaAuditing
@EnableConfigurationProperties({
    FileUploadProperties.class
})
public class ToursAndTravelsApplication {
	
	private static final Logger log = LoggerFactory.getLogger(ToursAndTravelsApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(ToursAndTravelsApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(UserService srv) {
	    return (args) -> {
	    	if(srv.getAllUsers().size()==0) {
	    		User user=new User();
	    		user.setUserid("admin");
	    		user.setPwd("admin123");
	    		user.setRole("Admin");
	    		user.setUname("Administrator");
	    		srv.saveUser(user);
	    		log.info("Admin user created successfully");
	    	}
	    };
	}
}
