package com.tours.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.tours.dtos.PlaceDTO;
import com.tours.dtos.Response;
import com.tours.models.Flight;
import com.tours.models.Hotel;
import com.tours.models.TourPackage;
import com.tours.services.BookingService;
import com.tours.services.FlightService;
import com.tours.services.HotelService;
import com.tours.services.PlaceService;
import com.tours.services.TourPackageService;
import com.tours.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("api/admin")
public class AdminController {

	@Autowired
	UserService uservice;
	@Autowired
	BookingService bservice;
	@Autowired
	HotelService hservice;
	@Autowired
	FlightService fservice;
	@Autowired
	PlaceService pservice;
	@Autowired
	TourPackageService tservice;

	@GetMapping("/bookings")
	public ResponseEntity<?> bookings(String userid) {
		if (StringUtils.hasLength(userid)) {
			return ResponseEntity.ok(bservice.getMyBookings(userid));
		}
		return ResponseEntity.ok(bservice.getAllBookings());
	}

	@GetMapping("/users")
	public ResponseEntity<?> usersList(Model model) {
		return ResponseEntity.ok(uservice.getAllUsers());
	}

	@PostMapping("/hotels")
	public ResponseEntity<?> saveHotel(@RequestBody Hotel hotel) {
		hservice.saveHotel(hotel);
		return Response.success("Hotel added successfully");
	}

	@DeleteMapping("/hotels/{id}")
	public ResponseEntity<?> deleteHotel(@PathVariable("id") int id) {
		hservice.deleteHotel(id);
		return Response.success("Hotel deleted successfully");
	}

	@GetMapping("/hotels/{id}")
	public ResponseEntity<?> getHotel(@PathVariable("id") int id) {
		return Response.success(hservice.findById(id));
	}

	@PostMapping("/flights")
	public ResponseEntity<?> flights(@RequestBody Flight flight) {
		fservice.saveFlight(flight);
		return Response.success("Flight added successfully");
	}

	@DeleteMapping("/flights/{id}")
	public ResponseEntity<?> deleteFlight(@PathVariable("id") int id) {
		fservice.deleteFlight(id);
		return Response.success("Flight deleted successfully");
	}

	@GetMapping("/flights/{id}")
	public ResponseEntity<?> getFlightDetails(@PathVariable("id") int id) {
		Flight flight = fservice.findById(id);
		return Response.success(flight);
	}

	@PostMapping("/places")
	public ResponseEntity<?> places(PlaceDTO dto) {
		pservice.savePlace(dto);
		return Response.success("Place added successfully");
	}

	@DeleteMapping("/places/{id}")
	public ResponseEntity<?> deletePlace(@PathVariable("id") int id) {
		pservice.deletePlace(id);
		return Response.success("Place deleted successfully");
	}

	@PostMapping("/packages")
	public ResponseEntity<?> tours(TourPackage pkg, MultipartFile pic) {
		tservice.savePakage(pkg, pic);
		return Response.success("Package added successfully");
	}

	@DeleteMapping("/packages/{id}")
	public ResponseEntity<?> deletePackages(@PathVariable("id") int id) {
		tservice.deletePackage(id);
		return Response.success("Package deleted successfully");
	}

	@GetMapping("/packages/{id}")
	public ResponseEntity<?> getPackages(@PathVariable("id") int id) {
		return Response.success(tservice.findById(id));
	}

	@DeleteMapping("/bookings/{id}")
	public ResponseEntity<?> cancelBooking(@PathVariable("id") int id) {
		bservice.cancelBooking(id);
		return Response.success("Booking cancelled successfully");
	}

}
