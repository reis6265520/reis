package com.tours.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tours.dtos.LoginDTO;
import com.tours.dtos.Response;
import com.tours.exceptions.InvalidUserException;
import com.tours.models.Booking;
import com.tours.models.Payment;
import com.tours.models.User;
import com.tours.services.BookingService;
import com.tours.services.FlightService;
import com.tours.services.HotelService;
import com.tours.services.PlaceService;
import com.tours.services.TourPackageService;
import com.tours.services.UserService;

@RestController
@CrossOrigin
@RequestMapping("api/users")
public class UserController {
	
	@Autowired UserService uservice;
	@Autowired BookingService bservice;
	@Autowired HotelService hservice;
	@Autowired FlightService fservice;
	@Autowired TourPackageService tservice;
	@Autowired PlaceService pservice;
	
	@PostMapping("/book")
	public ResponseEntity<?> savebooking(@RequestBody Booking book) {
		System.out.println(book);
		Booking bk= bservice.saveBooking(book);
		return ResponseEntity.ok(bk);
	}
	
	
	@GetMapping("/bookings/{id}")
	public ResponseEntity<?> payments(@PathVariable("id") int bid) {
		System.out.println("booking "+bid);
		Booking bk=bservice.findById(bid);
		System.out.println(bk);
		return ResponseEntity.ok(bk);
	}
	
	@PostMapping("/payment")
	public ResponseEntity<?> savePayment(@RequestBody Payment pmt) {
		bservice.savePayment(pmt);
		return Response.success("Booking successfully");
	}
	
	@PostMapping("/validate")
	public ResponseEntity<?> validate(@RequestBody LoginDTO dto) throws InvalidUserException {
		User user=uservice.ValidateLogin(dto);
		if(user==null) {
			throw new InvalidUserException();
		}else {
			return ResponseEntity.ok(user);			
		}
	}
	
	@PostMapping("/register")
	public ResponseEntity<?> saveuser(@RequestBody User user) {
		uservice.saveUser(user);
		return Response.success("User registered successfully");
	}
	
	@GetMapping("/hotels")
	public ResponseEntity<?> hotels(Model model) {
		return ResponseEntity.ok(hservice.listall());
	}
	
	@GetMapping("/places")
	public ResponseEntity<?> places(Model model) {
		return ResponseEntity.ok(pservice.listAll());
	}
	
	
	@GetMapping("/packages")
	public ResponseEntity<?> packages(Model model) {
		return ResponseEntity.ok(tservice.listall());
	}
	
	@GetMapping("/flights")
	public ResponseEntity<?> flights(Model model) {
		return ResponseEntity.ok(fservice.listall());
	}
	
}
