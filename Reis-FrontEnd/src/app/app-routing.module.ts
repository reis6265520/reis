import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddFlightComponent } from './pages/add-flight/add-flight.component';
import { AddHotelComponent } from './pages/add-hotel/add-hotel.component';
import { AddPackageComponent } from './pages/add-package/add-package.component';
import { AddPlaceComponent } from './pages/add-place/add-place.component';
import { BookingsComponent } from './pages/bookings/bookings.component';
import { FlightsComponent } from './pages/flights/flights.component';
import { HomeComponent } from './pages/home/home.component';
import { HotelsComponent } from './pages/hotels/hotels.component';
import { LoginComponent } from './pages/login/login.component';
import { PackagesComponent } from './pages/packages/packages.component';
import { PlacesComponent } from './pages/places/places.component';
import { RegisterComponent } from './pages/register/register.component';
import { UsersComponent } from './pages/users/users.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { AllbookingsComponent } from './pages/allbookings/allbookings.component';
import { AuthGuard } from './auth.guard';
import { EditFlightComponent } from './pages/edit-flight/edit-flight.component';
import { EditPackageComponent } from './pages/edit-package/edit-package.component';
import { EditHotelComponent } from './pages/edit-hotel/edit-hotel.component';
import { NgxCaptchaModule } from 'ngx-captcha';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';



const routes: Routes = [
  {path:"", component:HomeComponent,pathMatch:'full'},
  {path:"flights", component:FlightsComponent},
  {path:"places", component:PlacesComponent},
  {path:"packages", component:PackagesComponent},
  {path:"login", component:LoginComponent},
  {path:"register", component:RegisterComponent},
  {path:"hotels", component:HotelsComponent},
  {path:"users", component:UsersComponent},
  {path:"book/:id", component:BookingsComponent,canActivate:[AuthGuard]},
  {path:"payments/:bid", component:PaymentComponent,canActivate:[AuthGuard]},
  {path:"add-hotel", component:AddHotelComponent},
  {path:"edit-hotel/:id", component:EditHotelComponent},
  {path:"add-flight", component:AddFlightComponent},
  {path:"edit-flight/:id", component:EditFlightComponent},
  {path:"add-package", component:AddPackageComponent},
  {path:"edit-package/:id", component:EditPackageComponent},
  {path:"add-place", component:AddPlaceComponent},
  {path:"bookings", component:AllbookingsComponent,canActivate:[AuthGuard]}
];

@NgModule({
  // declarations: [
  //   AppComponent,
  //   LoginComponent
  // ],

  imports: [RouterModule.forRoot(routes),NgxCaptchaModule,FormsModule,ReactiveFormsModule],
  
  
  exports: [RouterModule]
})
export class AppRoutingModule { }
