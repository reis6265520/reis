import { map } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  register(user:any){
    return this.http.post<any>(environment.baseUrl+'api/users/register',user)
  }

  getUsers(){
    return this.http.get<User[]>(environment.baseUrl+'api/admin/users').pipe(map(users=>users.filter(user=>user.role!='Admin')))
  }

  validate(info:any){
    return this.http.post<User>(environment.baseUrl+'api/users/validate',info)
  }

  saveHotel(data:any){
    return this.http.post<any>(environment.baseUrl+'api/admin/hotels',data)
  }

  deleteHotel(pid:number){
    return this.http.delete<any>(environment.baseUrl+'api/admin/hotels/'+pid);
  }

  getHotels(){
    return this.http.get<any>(environment.baseUrl+'api/users/hotels')
  }

  getHotelDetails(id:number){
    return this.http.get<any>(environment.baseUrl+'api/admin/hotels/'+id)
  }

  saveFlight(data:any){
    return this.http.post<any>(environment.baseUrl+'api/admin/flights',data)
  }

  getFlights(){
    return this.http.get<any>(environment.baseUrl+'api/users/flights')
  }

  getFlightDetails(id:number){
    return this.http.get<any>(environment.baseUrl+'api/admin/flights/'+id)
  }

  deleteFlight(pid:number){
    return this.http.delete<any>(environment.baseUrl+'api/admin/flights/'+pid);
  }


  savePlace(data:any){
    return this.http.post<any>(environment.baseUrl+'api/admin/places',data)
  }

  getPlaces(){
    return this.http.get<any>(environment.baseUrl+'api/users/places')
  }

  deletePlace(pid:number){
    return this.http.delete<any>(environment.baseUrl+'api/admin/places/'+pid);
  }

  savePackage(data:any){
    return this.http.post<any>(environment.baseUrl+'api/admin/packages',data)
  }

  getPackages(){
    return this.http.get<any>(environment.baseUrl+'api/users/packages')
  }

  deletePackage(pid:number){
    return this.http.delete<any>(environment.baseUrl+'api/admin/packages/'+pid);
  }

  getPackageDetails(id:number){
    return this.http.get<any>(environment.baseUrl+'api/admin/packages/'+id)
  }

  booknow(data:any){
    return this.http.post<any>(environment.baseUrl+'api/users/book',data);
  }

  getBookingDetails(id:number){
    return this.http.get<any>(environment.baseUrl+'api/users/bookings/'+id)
  }

  makePayment(data:any){
    return this.http.post<any>(environment.baseUrl+'api/users/payment',data);
  }

  getAllBookings(){
    return this.http.get<any>(environment.baseUrl+'api/admin/bookings')
  }

  getUserBookings(userid:string){
    return this.http.get<any>(environment.baseUrl+'api/admin/bookings?userid='+userid)
  }

  cancelBookings(id:number){
    return this.http.delete<any>(environment.baseUrl+'api/admin/bookings/'+id)
  }
}
