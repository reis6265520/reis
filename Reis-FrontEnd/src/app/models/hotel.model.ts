export interface Hotel {
    id: number;
    hname: string;
    fare: number;
    city: string;
    state: string;
    country: string;
}