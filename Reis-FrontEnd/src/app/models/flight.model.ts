export interface Flight {
    id: number
    fare: number
    fname: string
    fromcity: string
    tocity: string
    fromstate: string
    fromcountry: string
    tostate: string
    tocountry: string
  }