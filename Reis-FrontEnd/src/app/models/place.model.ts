export interface Place{
    id: number
    pname: string
    description: string
    city: string
    state: string
    country: string
    photo: string
}