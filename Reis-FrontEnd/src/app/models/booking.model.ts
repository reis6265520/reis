import { Flight } from "./flight.model"
import { Hotel } from "./hotel.model"
import { Tour } from "./tour.model"
import { User } from "./user.model"

export interface Booking {
    bid: number
    flight: Flight
    tour: Tour
    hotel: Hotel
    insurance: boolean
    bdate: string
    user: User
    status: string
    
}