export interface User {
    userid: string;
    uname: string;
    pwd: string;
    gender?: any;
    address?: any;
    phone?: any;
    role: string;
}