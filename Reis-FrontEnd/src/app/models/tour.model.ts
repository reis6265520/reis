export interface Tour {
    pid: number
    pname: string
    photo: string
    price: number
    city: string
    state: string
    country: string
  }