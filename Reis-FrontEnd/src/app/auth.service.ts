import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {BehaviorSubject, Subject} from 'rxjs';
import { User } from './models/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn=new BehaviorSubject<boolean>(false);
  isAdmin=new BehaviorSubject<boolean>(false);
  user=new BehaviorSubject<User|null>(null)
  
  constructor(private http:HttpClient) { }
}
