import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  uinfo!:User;
  constructor(public auth:AuthService,private router:Router) { }

  ngOnInit(): void {
    let user=localStorage.getItem('user-data')
    if(user){
      this.uinfo=JSON.parse(user)
      this.auth.isAdmin.next(this.uinfo.role==='Admin')
      this.auth.isLoggedIn.next(true)
    }
  }

  logout(){
    localStorage.clear()
    this.auth.isAdmin.next(false)
    this.auth.isLoggedIn.next(false)
    this.router.navigateByUrl('')
  }

}
