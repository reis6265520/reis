import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormBuilder,Validators } from "@angular/forms";
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';
import { AlertifyService } from 'src/app/alertify.service';
import { takeUntil } from 'rxjs';
import { BaseComponent } from 'src/app/base.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent extends BaseComponent implements OnInit {
  protected aFormGroup!: FormGroup;
  


  loginForm!:FormGroup;
  
  constructor(private auth:AuthService,private api:ApiService,private router:Router,private fb:FormBuilder,private alert:AlertifyService ) { 
    super()
    
  }

  ngOnInit(): void {
    this.createForm()
  }

  createForm(){
    this.loginForm=this.fb.group({
      'userid':['',Validators.required],
      'pwd':['',Validators.required]
    })
  }

  validateForm(){
    if(this.loginForm.valid){      
      this.api.validate(this.loginForm.value).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          console.log(resp)
          this.auth.isLoggedIn.next(true)
          this.auth.user.next(resp)
          this.auth.isAdmin.next(resp.role==="Admin")
          localStorage.setItem('user-data',JSON.stringify(resp))
          this.alert.success('Welcome! '+resp.uname)
          this.router.navigateByUrl('')
        },
        error:err=>{
          console.log(err.error.message)
          this.alert.error('Invalid username or password..!!')
        }
      })
    }else{
        this.alert.error('Please fill all required fields..!!')
    }
  }
  
}

