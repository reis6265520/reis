import { ApiService } from 'src/app/api.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { AlertifyService } from 'src/app/alertify.service';
import * as alertify from 'alertifyjs';
@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.scss']
})
export class PlacesComponent implements OnInit {

  places!:any[];
  constructor(private api:ApiService,public auth:AuthService,private alert:AlertifyService) { }

  ngOnInit(): void {
    this.api.getPlaces().subscribe(resp=>this.places=resp)
  }

  deletePlace(pid:number){
    alertify.confirm('Delete  Confirmation','Are you sure to delete this place ?',()=>{
      this.api.deletePlace(pid).subscribe(resp=>{
        this.alert.success('Deleted successfully...!!')
        this.api.getPlaces().subscribe(resp=>this.places=resp)
      })
    },
    ()=>{
      this.alert.error('Deletion aborted..!!')
    })
  }
}
