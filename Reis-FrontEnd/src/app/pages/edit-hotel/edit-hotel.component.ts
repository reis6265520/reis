import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { AlertifyService } from 'src/app/alertify.service';
import { BaseComponent } from 'src/app/base.component';
import { takeUntil } from 'rxjs';

@Component({
  selector: 'app-edit-hotel',
  templateUrl: './edit-hotel.component.html',
  styleUrls: ['./edit-hotel.component.scss']
})
export class EditHotelComponent extends BaseComponent implements OnInit {

  hotelForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private router:Router,private route:ActivatedRoute,private alert:AlertifyService) { 
    super()
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getHotelDetails(id);
  }

  getHotelDetails(id:number){
    this.api.getHotelDetails(id).subscribe(resp=>{
      this.createForm(resp.data)
    })
  }

  saveHotel():void{
    if(this.hotelForm.valid){
      this.api.saveHotel(this.hotelForm.value).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          this.router.navigate(['/hotels'])
          this.alert.success('Hotel updated successfully..!')
        },
        error:err=>this.alert.error(err.error.message)
      })
    }else{

    }
  }

  createForm(item:any){
    this.hotelForm=this.fb.group({
      'id':[item.id],
      'hname':[item.hname,Validators.required],
      'fare':[item.fare,Validators.required],
      'city':[item.city,Validators.required],
      'state':[item.state,Validators.required],
      'country':[item.country,Validators.required],
    })
  }

}
