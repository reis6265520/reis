import { ApiService } from 'src/app/api.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import * as alertify from 'alertifyjs'; 
import { AlertifyService } from 'src/app/alertify.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-hotels',
  templateUrl: './hotels.component.html',
  styleUrls: ['./hotels.component.scss']
})
export class HotelsComponent implements OnInit {

  hotels!:any[]
  constructor(public auth:AuthService, private api:ApiService, private router: Router, private alert:AlertifyService) { }

  ngOnInit(): void {
    this.api.getHotels().subscribe({
      next:resp=>this.hotels=resp,
      error:err=>alert(err)
    })
  }

  editHotel(id:number){
    this.router.navigate(['/edit-hotel/'+id])
  }

  deleteHotel(id:number){
    alertify.confirm('Delete  Confirmation','Are you sure to delete this hotel ?',()=>{
      this.api.deleteHotel(id).subscribe(resp=>{
        this.alert.success('Deleted successfully...!!')
        this.api.getHotels().subscribe(resp=>this.hotels=resp)
      })
    },
    ()=>{
      this.alert.error('Deletion aborted..!!')
    })
  }
}
