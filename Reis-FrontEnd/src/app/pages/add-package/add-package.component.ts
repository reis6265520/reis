import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
import { BaseComponent } from 'src/app/base.component';

@Component({
  selector: 'app-add-package',
  templateUrl: './add-package.component.html',
  styleUrls: ['./add-package.component.scss']
})
export class AddPackageComponent extends BaseComponent implements OnInit {

  packageForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private router:Router,private alert:AlertifyService) {
    super()
   }
  preview!:string;

  ngOnInit(): void {
    this.createForm()
  }

  savePackage():void{
    if(this.packageForm.valid){
      const fd=new FormData()
      Object.keys(this.packageForm.value).forEach(key=>{
        console.log(key)
        fd.append(key,this.packageForm.value[key])
      })
      console.log(fd)
    
      this.api.savePackage(fd).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          this.router.navigate(['/packages'])
          this.alert.success(resp.data)
        },
        error:err=>this.alert.error(err.error.message)
      })
    }
  }

  uploadFile(event:any) {
    const file = event.target?.files[0] as File;
    this.packageForm.patchValue({
      pic: file
    });
    //this.packageForm?.get('pic').updateValueAndValidity()
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file)
  }


  createForm(){
    this.packageForm=this.fb.group({
      'pname':['',Validators.required],
      'price':['',Validators.required],
      'city':['',Validators.required],
      'state':['',Validators.required],
      'country':['',Validators.required],
      'pic':[null]
    })
  }
    
}
