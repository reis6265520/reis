import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, map, takeUntil } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';
import { BaseComponent } from 'src/app/base.component';
import { Flight } from 'src/app/models/flight.model';
import { Hotel } from 'src/app/models/hotel.model';
import { Tour } from 'src/app/models/tour.model';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.component.html',
  styleUrls: ['./bookings.component.scss']
})
export class BookingsComponent extends BaseComponent implements OnInit {

  flights$!:Observable<Flight[]>
  tours$!:Observable<Tour[]>
  hotels$!:Observable<Hotel[]>
  pkgid!:any
  bookForm!:FormGroup
  issubmitted:boolean=false
  uinfo!:User
  bookingdata:any;

  constructor(private api:ApiService,
    private acroute:ActivatedRoute,
    public auth:AuthService,
    private fb:FormBuilder,
    private route:Router,
    private alert:AlertifyService) {
      super()
     }

  ngOnInit(): void {
    this.loadData()
    this.pkgid=this.acroute.snapshot.paramMap.get('id')
    let user=localStorage.getItem('user-data')
    if(user){
      this.uinfo=JSON.parse(user);
      this.createForm()    
      this.bookForm.patchValue({'tour':this.pkgid,'user':this.uinfo.userid})
    }else{
      this.alert.error('Please login first to booking...!!')
      this.route.navigateByUrl('/login')
    }
  }

  saveBooking(){
    if(this.bookForm.valid){
    this.api.booknow(this.bookForm.value).subscribe({
      next:resp=>
      {
        this.issubmitted=true
        this.api.getBookingDetails(resp.bid).pipe(takeUntil(this.destroy$)).subscribe({
          next:resp=>this.route.navigate(['/payments',resp.bid]),
          error:err=>console.log(err)
        })
      },
      error:err=>console.log("error",err)
    })
  }
  else{
    this.alert.error('Please fill all required fields')
  }
  }

  createForm(){
    this.bookForm=this.fb.group({
      'tour':['',Validators.required],
      'flight':['',Validators.required],
      'hotel':['',Validators.required],
      'bdate':['',Validators.required],
      'insurance':[''],
      'user':['',Validators.required]
    })
  }

  loadData(){
    this.flights$= this.api.getFlights()
    this.hotels$= this.api.getHotels()
    this.tours$= this.api.getPackages()
  }

}
