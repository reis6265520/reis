import { HttpEvent } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-add-place',
  templateUrl: './add-place.component.html',
  styleUrls: ['./add-place.component.scss']
})
export class AddPlaceComponent implements OnInit {

  placeForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private router:Router,private alert:AlertifyService) { }
  preview!:string;
  ngOnInit(): void {
    this.createForm()
  }

  uploadFile(event:any) {
    const file = event.target?.files[0] as File;
    this.placeForm.patchValue({
      pic: file
    });
    //this.placeForm?.get('pic').updateValueAndValidity()
    // File Preview
    const reader = new FileReader();
    reader.onload = () => {
      this.preview = reader.result as string;
    }
    reader.readAsDataURL(file)
  }

  savePlace():void{
    if(this.placeForm.valid){
      const fd=new FormData()
      Object.keys(this.placeForm.value).forEach(key=>{
        console.log(key)
        fd.append(key,this.placeForm.value[key])
      })
      console.log(fd)
    
      this.api.savePlace(fd).subscribe({
        next:resp=>{
          this.router.navigate(['/places'])
          this.alert.success(resp.data)
        },
        error:err=>this.alert.error(err.error.message)
      })
    }
  }

  createForm(){
    this.placeForm=this.fb.group({
      'pname':['',Validators.required],
      'description':['',Validators.required],
      'city':['',Validators.required],
      'state':['',Validators.required],
      'country':['',Validators.required],
      'pic':[null]
    })
  }

}
