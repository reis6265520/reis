import { Component, OnInit } from '@angular/core';
import { Observable, map } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';
import { Booking } from 'src/app/models/booking.model';
import { User } from 'src/app/models/user.model';
import * as alertify from 'alertifyjs';

@Component({
  selector: 'app-allbookings',
  templateUrl: './allbookings.component.html',
  styleUrls: ['./allbookings.component.scss']
})
export class AllbookingsComponent implements OnInit {

  bookings$!:Observable<Booking[]>
  user!:User
  constructor(private api:ApiService,private auth:AuthService,private alert:AlertifyService) { }

  ngOnInit(): void {
      this.loadData();
  }

  loadData(){
    const uinfo=localStorage.getItem("user-data")
    if(uinfo){
      this.user=JSON.parse(uinfo)
      if(this.user.role=="Admin"){
        this.bookings$=this.api.getAllBookings()
      }else{
        this.bookings$=this.api.getUserBookings(this.user.userid)
      }
    }  
  }

  cancelBooking(id:number){
    alertify.confirm('Delete  Confirmation','Are you sure to cancel this booking ?',()=>{
      this.api.cancelBookings(id).subscribe(()=>{
        this.alert.success('Cancelled successfully...!!')
        this.loadData();
      })
    },
    ()=>{
      this.alert.error('Cancellation aborted..!!')
    })
  }

}
