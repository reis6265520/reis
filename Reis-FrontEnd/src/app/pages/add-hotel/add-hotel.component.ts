import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { AlertifyService } from 'src/app/alertify.service';
import { BaseComponent } from 'src/app/base.component';
import { takeUntil } from 'rxjs';

@Component({
  selector: 'app-add-hotel',
  templateUrl: './add-hotel.component.html',
  styleUrls: ['./add-hotel.component.scss']
})
export class AddHotelComponent extends BaseComponent implements OnInit {

  hotelForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private router:Router,private alert:AlertifyService) { 
    super()
  }

  ngOnInit(): void {
    this.createForm()
  }

  saveHotel():void{
    if(this.hotelForm.valid){
      this.api.saveHotel(this.hotelForm.value).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          this.router.navigate(['/hotels'])
          this.alert.success(resp.data)
        },
        error:err=>this.alert.error(err.error.message)
      })
    }else{

    }
  }

  createForm(){
    this.hotelForm=this.fb.group({
      'hname':['',Validators.required],
      'fare':['',Validators.required],
      'city':['',Validators.required],
      'state':['',Validators.required],
      'country':['',Validators.required],
    })
  }
}
