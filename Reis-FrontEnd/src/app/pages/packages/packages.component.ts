import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';
import * as alertify from 'alertifyjs';
import { AlertifyService } from 'src/app/alertify.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-packages',
  templateUrl: './packages.component.html',
  styleUrls: ['./packages.component.scss']
})
export class PackagesComponent implements OnInit {

  packages!:any[]
  constructor(private api:ApiService, public auth:AuthService, private router:Router, private alert:AlertifyService) { }

  ngOnInit(): void {
    this.api.getPackages().subscribe({
      next:resp=>this.packages=resp,
      error:err=>console.log("Error",err)
    })
  }

  editPackage(id:number){
    this.router.navigate(['/edit-package/'+id])
  }

  deletePackage(event:Event,id:number){
    event.stopPropagation()
    alertify.confirm('Delete  Confirmation','Are you sure to delete this Package ?',()=>{
      this.api.deletePackage(id).subscribe(resp=>{
        this.alert.success('Deleted successfully...!!')
        this.api.getPackages().subscribe(resp=>this.packages=resp)
      },
      error=>console.log(error))
    },
    ()=>{
      this.alert.error('Deletion aborted..!!')
    })
    
  }
}
