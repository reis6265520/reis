import { ApiService } from 'src/app/api.service';
import { Component, OnInit } from '@angular/core';
import * as alertify from 'alertifyjs';
import { AuthService } from 'src/app/auth.service';
import { AlertifyService } from 'src/app/alertify.service';
import { Observable } from 'rxjs';
import { Flight } from 'src/app/models/flight.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-flights',
  templateUrl: './flights.component.html',
  styleUrls: ['./flights.component.scss']
})
export class FlightsComponent implements OnInit {

  flights$!:Observable<Flight[]>
  constructor(public auth:AuthService, private api:ApiService,private alert:AlertifyService, private router:Router) { }

  ngOnInit(): void {
    this.flights$= this.api.getFlights()
  }

  editFlight(id:number){
    this.router.navigate(['/edit-flight/'+id])
  }

  deleteFlight(id:number){
    alertify.confirm('Delete  Confirmation','Are you sure to delete this Flight ?',()=>{
      this.api.deleteFlight(id).subscribe(()=>{
        this.alert.success('Deleted successfully...!!')
        this.flights$= this.api.getFlights()
      })
    },
    ()=>{
      this.alert.error('Deletion aborted..!!')
    })
  }
}
