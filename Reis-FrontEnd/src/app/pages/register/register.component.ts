import { Component, OnInit } from '@angular/core';
import {FormGroup,FormBuilder,Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tap } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  fg!:FormGroup;
  constructor(private fb:FormBuilder,private api:ApiService,private route:Router,private alert:AlertifyService) { }

  ngOnInit(): void {
    this.createForm()
  }

  createForm(){
    this.fg=this.fb.group({
      uname:['',Validators.required],
      userid:['',Validators.required],
      pwd:['',Validators.required],
      gender:['',Validators.required],
      address:['',Validators.required],
      phone:['',Validators.required],
    })
  }

  submit(){
    console.log(this.fg.valid)
    if(this.fg.valid){
      console.log(this.fg.value)
      this.api.register(this.fg.value).pipe(
        tap(resp=>console.log(resp))        
        )
        this.alert.success('Registered successfully..!!')
        this.route.navigate(['login'])
      this.api.register(this.fg.value).subscribe({
        next:()=>
        {
          this.alert.success('Registered successfully..!!')
          this.route.navigate(['login'])
        },
        error:err=>console.log('Error',err)
      })
    }
    else
      this.alert.error('Please fill all fields')
  }
}
