import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
import { BaseComponent } from 'src/app/base.component';

@Component({
  selector: 'app-add-flight',
  templateUrl: './add-flight.component.html',
  styleUrls: ['./add-flight.component.scss']
})
export class AddFlightComponent extends BaseComponent implements OnInit {

  flightForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private router:Router,private alert:AlertifyService) { 
    super()
  }

  ngOnInit(): void {
    this.createForm()
  }

  saveFlight():void{
    if(this.flightForm.valid){
      this.api.saveFlight(this.flightForm.value).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          this.router.navigate(['/flights'])
          this.alert.success(resp.data)
        },
        error:err=>this.alert.error(err.error.message)
      })
    }
  }

  createForm(){
    this.flightForm=this.fb.group({
      'fname':['',Validators.required],
      'fare':['',Validators.required],
      'fromcity':['',Validators.required],
      'fromstate':['',Validators.required],
      'fromcountry':['',Validators.required],
      'tocity':['',Validators.required],
      'tostate':['',Validators.required],
      'tocountry':['',Validators.required],
    })
  }

}
