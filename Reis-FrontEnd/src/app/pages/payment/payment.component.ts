import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss']
})
export class PaymentComponent implements OnInit {

  bookinginfo:any;
  bid!:number
  totalbill!:number
  paymentForm!:FormGroup
  uinfo!:User
  constructor(private api:ApiService,private acroute:ActivatedRoute,private router:Router,private fb:FormBuilder) { }

  ngOnInit(): void {
    this.acroute.params.subscribe(resp=>this.bid=resp['bid'])
    let user=localStorage.getItem('user-data')
    if(user){
      this.uinfo=JSON.parse(user);
    }
    this.createForm()
    this.api.getBookingDetails(this.bid).subscribe({
      next:resp=>{
        this.bookinginfo=resp
        this.totalbill=this.bookinginfo.tour.price+this.bookinginfo.flight.fare+this.bookinginfo.hotel.fare+(this.bookinginfo.insurance ? 1000 : 0)
        this.paymentForm.patchValue({'amount':this.totalbill})
      }
    })
  }

  makePayment(){
    console.log(this.paymentForm.value)
    if(this.paymentForm.valid){
      this.api.makePayment(this.paymentForm.value).subscribe(
        {
          next:resp=> {
            alert('Booking successfully')
            this.router.navigate(['bookings'])
          },
          error:err=>alert(err)
        }
      )
    }
  }

  createForm(){
    this.paymentForm=this.fb.group({
      'booking':[this.bid,Validators.required],
      'user':[this.uinfo.userid],
      'amount':['',Validators.required],
      'mode':['',Validators.required]
    })
  }

}
