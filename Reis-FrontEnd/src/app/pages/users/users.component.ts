import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { User } from 'src/app/models/user.model';
import {Observable, filter,map} from 'rxjs';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent {

  users$!:Observable<User[]>
  
  constructor(private api:ApiService) { 
    this.users$=this.api.getUsers()
  }

  

}
