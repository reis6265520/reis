import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { takeUntil } from 'rxjs';
import { AlertifyService } from 'src/app/alertify.service';
import { ApiService } from 'src/app/api.service';
import { BaseComponent } from 'src/app/base.component';

@Component({
  selector: 'app-edit-flight',
  templateUrl: './edit-flight.component.html',
  styleUrls: ['./edit-flight.component.scss']
})
export class EditFlightComponent extends BaseComponent implements OnInit {

  flightForm!:FormGroup;
  constructor(private api:ApiService,private fb:FormBuilder,private route:ActivatedRoute, private router:Router,private alert:AlertifyService) { 
    super()
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.getflightDetails(id);
  }

  getflightDetails(id:number){
    this.api.getFlightDetails(id).subscribe(resp=>{
      this.createForm(resp.data)
    })
  }

  saveFlight():void{
    if(this.flightForm.valid){
      this.api.saveFlight(this.flightForm.value).pipe(takeUntil(this.destroy$)).subscribe({
        next:resp=>{
          this.router.navigate(['/flights'])
          this.alert.success('Flight updated successfully')
        },
        error:err=>this.alert.error(err.error.message)
      })
    }
  }

  createForm(item:any){
    this.flightForm=this.fb.group({
      'id':[item.id],
      'fname':[item.fname,Validators.required],
      'fare':[item.fare,Validators.required],
      'fromcity':[item.fromcity,Validators.required],
      'fromstate':[item.fromstate,Validators.required],
      'fromcountry':[item.fromcountry,Validators.required],
      'tocity':[item.tocity,Validators.required],
      'tostate':[item.tostate,Validators.required],
      'tocountry':[item.tocountry,Validators.required],
    })
  }

}
