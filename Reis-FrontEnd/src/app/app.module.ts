import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { PackagesComponent } from './pages/packages/packages.component';
import { PlacesComponent } from './pages/places/places.component';
import { HotelsComponent } from './pages/hotels/hotels.component';
import { FlightsComponent } from './pages/flights/flights.component';
import { BookingsComponent } from './pages/bookings/bookings.component';
import { AllbookingsComponent } from './pages/allbookings/allbookings.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { UsersComponent } from './pages/users/users.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AddHotelComponent } from './pages/add-hotel/add-hotel.component';
import { AddFlightComponent } from './pages/add-flight/add-flight.component';
import { AddPlaceComponent } from './pages/add-place/add-place.component';
import { AddPackageComponent } from './pages/add-package/add-package.component';
import { AuthGuard } from './auth.guard';
import { EditFlightComponent } from './pages/edit-flight/edit-flight.component';
import { EditPackageComponent } from './pages/edit-package/edit-package.component';
import { EditHotelComponent } from './pages/edit-hotel/edit-hotel.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PackagesComponent,
    PlacesComponent,
    HotelsComponent,
    FlightsComponent,
    BookingsComponent,
    AllbookingsComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    UsersComponent,
    PaymentComponent,
    AddHotelComponent,
    AddFlightComponent,
    AddPlaceComponent,
    AddPackageComponent,
    EditFlightComponent,
    EditPackageComponent,
    EditHotelComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
