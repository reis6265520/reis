import { Injectable } from '@angular/core';
import * as alertify from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

  constructor() { }

  success(msg:string){
    alertify.set('notifier','position', 'top-center');
    alertify.success(msg);
  }

  error(msg:string){
    alertify.set('notifier','position', 'top-center');
    alertify.error(msg);
  }
}
